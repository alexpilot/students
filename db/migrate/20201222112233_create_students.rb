class CreateStudents < ActiveRecord::Migration[6.0]
  def change
    create_table :students do |t|
      t.string :first_name, :limit => 40, null: false
      t.string :middle_name, :limit => 60
      t.string :last_name, :limit => 40, null: false
      t.integer :sex
      t.string :nick_name, unique: true


      t.timestamps
    end
  end
end
