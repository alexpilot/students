class Student < ApplicationRecord

  validates :first_name, length: { maximum: 40 }, presence: true
  validates :middle_name, length: { maximum: 60 }, presence: false
  validates :last_name, length: { maximum: 40 }, presence: true
  validates :sex, presence: true
  enum sex: ["male", "female"]
end
