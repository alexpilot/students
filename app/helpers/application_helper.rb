module ApplicationHelper
  def sortable(column, title = nil)
    title ||= column.titleize
    fasort = sort_direction == "asc" ? "down" : "up" 
    css_class = column == sort_col ? "current" : nil
    direction = column == sort_col && sort_direction == "asc" ? "desc" : "asc"
    link_to title, { :sort => column, :direction => direction }, { :class => css_class }
  end
end
