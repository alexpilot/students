class StudentsController < ApplicationController
  before_action :set_student, only: [ :show, :edit, :update, :destroy ]
  helper_method :sort_col, :sort_direction

  def index
    @students = Student.order( sort_col + ' ' + sort_direction )
    @student = Student.new
  end
  
  def new
    @student = Student.new
  end

  def show
  end

  def edit
    
  end

  def create
    @student = Student.new(student_params)

    respond_to do |format|
      if @student.save
        format.html { redirect_to @student, notice: "Student was successfully created." }
        format.json { render :show, status: :created, location: @student }
        format.js
      else
        format.html { render :new }
        format.json { render json: @student.errors, status: :bad_request}
        format.js
      end
    end
  end
  
  def update
    respond_to do |format|
      if @student.update(student_params)
        format.html { redirect_to @student, notice: "Student was successfully updated." }
        format.json { render :show, status: :ok, location: @student }
        format.js
      else
        format.html { render :edit }
        format.json { render json: @student.errors, status: :bad_request}
        format.js
      end
    end
  end

  def destroy
    @student.destroy
    respond_to do |format|
      format.html { redirect_to students_url, notice: 'Student was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private

  def set_student
    @student = Student.find(params[:id])
  end

  def student_params
    params.require(:student).permit(:first_name, :middle_name, :last_name, :sex, :nick_name)
  end

 def sort_col
   Student.column_names.include?( params[ :sort ] ) ? params[ :sort ] : "last_name"
 end

 def sort_direction
   %w[ asc desc ].include?( params[ :direction ] ) ? params[ :direction ] : "asc"
 end
end
